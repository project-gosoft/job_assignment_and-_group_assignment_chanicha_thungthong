package Library;

import java.util.ArrayList;
import java.util.Scanner;

public class Member {

	public String name; // Project งานกลุ่ม (Group Assignment)
	public String idMember;
	static ArrayList<Member> member = new ArrayList<>();
	static Scanner input = new Scanner(System.in);

	public Member(String nameInput, String idMemberInput) {
		this.idMember = idMemberInput;
		this.name = nameInput;
	}

	public static void addItemMember() {
		Member member1 = new Member("Kesinee", "S201"); // S : student
		Member member2 = new Member("Chanicha", "T101"); // T : teacher
		Member member3 = new Member("Achiraya", "F301"); // F : officer
		Member member4 = new Member("Thanachot", "F302");
		Member member5 = new Member("Jirawat", "L401"); // L : library staff

		member.add(member1);
		member.add(member2);
		member.add(member3);
		member.add(member4);
		member.add(member5);
	}

	// เพื่อเรียกรายการสมาชิกที่ถูกเก็บไว้ใน ArrayList มาแสดง
	public static void listMember() {
		if (member.size() == 0) {
			addItemMember();
		}
		System.out.println("List member");
		System.out.println(" ID           Title");
		for (Member listMember : member) {
			System.out.println("" + listMember);
		}
	}

	public static void editMember() {
		System.out.println("");
		if (member.size() == 0) {
			addItemMember();
		}
		int index = 0;
		System.out.println("List member");
		System.out.print("Index    ID                 Title");

		for (Member listName : member) {
			System.out.print("\n" + index + "    |  " + listName);
			index++;
		}
		System.out.println("");
		System.out.print("Please input your index want to edit : ");
		int indexEdit = input.nextInt();
		System.out.print("Please input your idMember want to edit :");
		String idMember = input.next();
		System.out.print("please input new title want to edit :  ");
		String nameMember = input.next();
		member.set(indexEdit, new Member(nameMember, idMember));
		System.out.println("");
		System.out.println(" ID           Title");
		if (member.size() > 3) {
			for (int i = 3; i <= member.size(); i++) {
				// System.out.println("i " + i);
				member.remove(i - 1);
			}
		}
		for (Member listMember2 : member) {
			System.out.println("" + listMember2);
		}
	}

	public static void deleteBook() {
		if (member.size() == 0) {
			addItemMember();
		}
		int index = 0;
		System.out.println("");
		System.out.println("Index    ID                 Title");

		for (Member listName : member) {
			System.out.print("\n" + index + "    |  " + listName);
			index++;
		}
		System.out.println("");
		System.out.print("Please input your index want to delete : ");
		int yourIdMember;
		boolean Program = false;

		while (Program == false)
			try {
				yourIdMember = Integer.parseInt(input.next());
				member.remove(yourIdMember);
				break;
			} catch (NumberFormatException nfe) {
				System.out.print("Please input idMember try again: ");
			}
		System.out.println("");
		for (Member listMember : member) {
			System.out.println("" + listMember);
		}
		// resetMember(member);
		Program = true;
	}

	public static void addMember() {
		if (member.size() == 0) {
			addItemMember();
		}

		System.out.println("");
		System.out.println("Index    ID                 Title");
		for (Member listMember2 : member) {
			System.out.println("" + listMember2);
		}
		System.out.println("");
		System.out.print("Please input your id Member want to add : ");
		String idMemberToAdd = input.next();
		boolean has = false;

		for (Member id : member) {
			while (id.idMember == idMemberToAdd) {
				System.out.println("Name Sum No ADD !!! ");
				System.out.print("Please input your id member want to add : ");
				idMemberToAdd = input.next();
			}
		}
		// resetMember(member);
		if (has == false) {
			System.out.print("Please input your name member want to add : ");
			String nameMember = input.next();
			Member addMember = new Member(nameMember, idMemberToAdd);
			member.add(addMember);
			listMember();
		}
	}

	public static void exitProgram() {
		System.out.println("Thank you for using library system. ");
	}

	@Override
	public String toString() {
		return " ID member : " + idMember + " | " + "Name : " + name;
	}
}
