import java.util.ArrayList;

import java.util.Scanner;
import java.util.Iterator;

public class TodoList {

	public static Scanner scan = new Scanner(System.in);
	public static ArrayList<Integer> com = new ArrayList<Integer>();

	public static void main(String[] args) {
		// Integer ArrayList

		com.add(1);
		com.add(2);
		com.add(3);
		com.add(4);
		System.out.print("List Infomation = " + com);
		System.out.println(
				"\n Select the desired case ? \n case 1 = Add \n case 2 = Remove \n case 3 = Edit \n case 4 = View");
		System.out.print("INPUT NUMBER CASE: ");
		int todo = scan.nextInt();
		switch (todo) {

		case 1: // สามารถเพิ่ม TODO List ได้
			System.out.print("Add todo list: ");
			int addtodo = scan.nextInt();
//
//			for (todo = 0; addtodo > 4; todo--) {
//			}
			add(addtodo);
			break;

		case 2: // สามารถลบ TODO List ได้;
			System.out.print("Input Todo List to Remove: ");
			int removetodo = scan.nextInt();
			remove(removetodo);
			break;


		case 3: // สามารถแก้ไข TODO List ได้
			System.out.print("Select TODO List to Edit: ");
			int edittodo = scan.nextInt();
			System.out.print("Input your new TODO List to edit : ");
			int edittodo2 = scan.nextInt();
			edit(edittodo, edittodo2);
			break;


		case 4: // สามารถเรียกดู TODO List ได้
			System.out.print("--View All TODO List = ");
			viewtodolist();
			break;

		default:
			System.out.println("Elevator don't know where to go.");
		}

	}

	public static void add(int addtodolist) {
		com.add(addtodolist);
		System.out.println("--Show TODO ADD = " + addtodolist);
		System.out.println("--Show TODO List = " + com);
	}
	
	public static void remove(int removetodo) {
		com.remove(removetodo - 1);
		System.out.println("--Show TODO List Remove : " + "[" + removetodo + "]" + " To Remove");
		System.out.println("--Show TODO List : " + com);
	}

	
	public static void edit(int edittodo, int edittodo2) {
		com.set(edittodo - 1, edittodo2);
		System.out.println("--Show TODO List Edit : " + "[" + edittodo + "]" + " To Edit is " + "[" + edittodo2 + "]");
		System.out.println("--Show TODO List : " + com);
	}

	public static void viewtodolist() {
		for (Iterator<Integer> iterator = com.iterator(); iterator.hasNext();) {
			Integer integer = (Integer) iterator.next();
			System.out.printf("[%-1d],",integer);
		}
	}
}
